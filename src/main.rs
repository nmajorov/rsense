
use sensehat::{Colour, SenseHat};
//use measurements::Temperature;

fn main() {
    if let Ok(mut hat) = SenseHat::new() {
        println!("{:?}", hat.get_pressure());

        let temp = hat.get_temperature_from_humidity().unwrap().as_celsius();
        
        println!("It's {} on the humidity sensor", temp);
        let temp2 = hat.get_temperature_from_pressure().unwrap().as_celsius();
        println!("It's {} on the pressure sensor", temp2);
        println!("That's a difference of {}", temp - temp2);
    }
}
